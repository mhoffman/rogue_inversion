clear all;

f=18
set(0,'DefaultTextFontSize', f, 'DefaultAxesFontSize', f)

%GPSDIR='/Users/mhoffman/documents/ROGUE/GPS_2011_processed/24hr'; fname_suffix='_xyz.mat';
GPSDIR='/Users/mhoffman/documents/ROGUE/GPS_2011_processed/6hr'; fname_suffix='_6h_xyz.mat';

stnlist={'19N1', '22N4', '25N1', '28N4', '33N1', '37N4', '38S3', '41N1', 'FOXX', 'GULL', 'HARE'};

lat=[
69.43818
69.47387
69.4455
69.48642
69.47051
69.52118
69.48456
69.51898
69.44621
69.45238
69.49313
];

lon=[
-49.92776
-49.87859
-49.78465
-49.74294
-49.64036
-49.60509
-49.47901
-49.47063
-49.88188
-49.7155
-49.54708
];

elev=[
625
730
836
876
927
988
1006
1039
667
852
993
];
[gpsx, gpsy] = polarstereo_fwd(lat, lon, 6378137, 0.0818191908426215, 70.0, -45.0);



stday = 152;
enday = 240;

% The time we want to grab out of the whole array
timeToGet = 181.0;  % before lake drainages
%timeToGet = 188.7;  % biggest lake drainage
timeToGet = 267.0;  % fall

timeToGet = 181.7;  % lake drainage in network
timeToGet = 182.2;  %  drainage in network
timeToGet = 184.2;  %  drainage in network
timeToGet = 183.2;  %  drainage in network
%timeToGet = 183.7;  %  drainage in network
timeToGet = 150.0;  % winter
timeToGet = 181.2;  % pre-lake drainage in network
timeToGet = 182.7;  % lake drainage in network - max

timeToGet = 182.6;  % lake drainage in network - max

% ===========================
% ===========================
timeToGet = dlmread('timestamp.txt');  % use this for automating this script
% ===========================
% ===========================



nt = (enday-stday+1);


figure(1); clf; hold all

alldata = zeros([22657, 3]);
c=jet(11);
for i=1:length(stnlist)
   load([GPSDIR '/' 'GPS2011_', stnlist{i}, fname_suffix]) 
   t = eval(['stn', stnlist{i}, '.t']);
   ux = eval(['stn', stnlist{i}, '.ux']);
   uy = eval(['stn', stnlist{i}, '.uy']);
   v = eval(['stn', stnlist{i}, '.v']);   
   
   plot( t,v, 'color', c(i,:), 'DisplayName',stnlist{i})
   
   allux(i,:) = ux;
   alluy(i,:) = uy;
   
   [junk, ind] = min(abs(t-timeToGet));  % get the index to the closest time
   data(i,1) = ux(ind);
   data(i,2) = uy(ind);  
end


save('gps_velo.mat','data');  % save for automating

legend show
xlabel('Day of year 2011')
ylabel('GPS speed (m/yr)')
dataspeed = (data(:,1).^2 + data(:,2).^2).^0.5;

allspeed = (allux.^2 + alluy.^2).^0.5;

% find times when all stn have data
complete=~isnan(sum(allspeed,1));
numgood=length(stnlist) - sum(isnan(allspeed),1);

%plot(t, (complete+1)*50.0, '.k')

%% velo plot cleaned up for paper
fontname = 'Helvetica';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

figure (101); clf;
w=6.5; h=4.5;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])
ax=subplot(1,1,1); hold all
set(gca,'FontSize',12)
set(gcf,'color','w');

c=jet(11);


GPSDIR='/Users/mhoffman/documents/ROGUE/GPS_2011_processed/6hr'; fname_suffix='_6h_xyz.mat';
for i=1:length(stnlist)
   load([GPSDIR '/' 'GPS2011_', stnlist{i}, fname_suffix]) 
   t = eval(['stn', stnlist{i}, '.t']);
   ux = eval(['stn', stnlist{i}, '.ux']);
   uy = eval(['stn', stnlist{i}, '.uy']);
   v = eval(['stn', stnlist{i}, '.v']);   
   
   plot( t,v, 'color', c(i,:), 'DisplayName',stnlist{i})
   legend_str{i}=stnlist{i};
   
   allux(i,:) = ux;
   alluy(i,:) = uy;
   
   [junk, ind] = min(abs(t-timeToGet));  % get the index to the closest time
   data(i,1) = ux(ind);
   data(i,2) = uy(ind);  
end



[legend_h,object_h,plot_h,text_strings] = columnlegend(2,legend_str,'Location','North');
%pos = get(legend_h, 'Position');
%set(legend_h, 'Position', [183, 400, pos(3), pos(4)]);
xlabel('Day of year 2011')
ylabel('GPS speed (m a-1)')%, 'Interpreter','LaTex')
box on
%plot([1 1]*178.25, [0 600], '--r')
%plot([1 1]*186.5833, [0 600], '--r')

patch([178.25, 186.5833, 186.5833, 178.25], [0 0 600 600], [1 1 1]*0.83, 'linestyle', 'none', 'facealpha', 0.5)

patch([182.2, 183.3, 183.3, 182.2], [0 0 600 600], [1 1 1]*0.75, 'linestyle', 'none', 'facealpha', 0.5)

patch([161.75, 162.3333, 162.3333, 161.75], [0 0 600 600], [1 1 1]*0.94, 'linestyle', 'none', 'facealpha', 0.5)
patch([164.1666-1/24, 164.25+1/24, 164.25+1/24, 164.1666-1/24], [0 0 600 600], [1 1 1]*0.94, 'linestyle', 'none', 'facealpha', 0.5)

xlim([158 187.5])
ylim([0 450])


print('-painters', 'velocity_gps_data', '-dpdf')

%%
figure(2); clf; hold all;



plot(t, nanmax(allspeed, 1) - nanmin(allspeed, 1) )
ylabel('max - min for all stations')



%% plot missing data
figure(32); clf; hold all
for i=1:length(stnlist)
   plot(t, isnan(allspeed(i,:)) * i, '.k')
end
plot(t, numgood, 'r')

title('missing data')
ylabel('stn number')
xlabel('DOY 2011')
set(gca,'YTick', [1:length(stnlist)]);
set(gca,'YTickLabel',stnlist)



%%  plot distance vs error for this day
figure(3); clf;

subplot(2,1,1);hold all;
for i=1:length(stnlist)
    for j=1:length(stnlist)
        d=( (gpsx(i)-gpsx(j))^2 + (gpsy(i)-gpsy(j))^2)^0.5/1000.0;
        plot(d, abs(dataspeed(i)-dataspeed(j)) / mean(dataspeed), 'kx')
    end
end
plot([0:20], 0.2 * [0:20].^0.5, 'r-')
plot([0:20], 0.35 * ([0:20]).^0.5, 'r:')
plot([0:20], 0.7 * (1 - exp(-3*[0:20]/10)), 'b-')
plot([0:20], 1.2 * (1 - exp(-3*[0:20]/10)), 'b-')

xlabel('distance, km')
ylabel('speed difference / network mean speed')

subplot(2,1,2);hold all;
for i=1:length(stnlist)
    for j=1:length(stnlist)
        d=( (gpsx(i)-gpsx(j))^2 + (gpsy(i)-gpsy(j))^2)^0.5/1000.0;
        plot(d, abs(dataspeed(i)-dataspeed(j)), 'kx')
    end
end
plot([0:20], 20 * [0:20].^0.5, 'r-')
xlabel('distance, km')
ylabel('speed difference (m/yr)')


%% build vtk points
p=0;
for i=1:length(stnlist)
    p=p+1;
    path3D(p,1)=gpsx(i);
    path3D(p,2)=gpsy(i);
    path3D(p,3)=0.0;

    p=p+1;
    path3D(p,1)=gpsx(i);
    path3D(p,2)=gpsy(i);
    path3D(p,3)=elev(i);
    
    p=p+1;
    path3D(p,1)=gpsx(i);
    path3D(p,2)=gpsy(i);
    path3D(p,3)=0.0;    

end 

path3D=path3D/1000.0;  % convert to km
 
export3Dline2VTK('gpsLocations',path3D,'/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson/albany_mesh/exodus/inversion')