clear all;


load Catania_bed.mat;
bed = map_data;
dx=500.0;
XX = [map_western_edge+dx/2.0:500.0:map_eastern_edge-dx/2.0];
YY = [map_southern_edge+dx/2.0:500.0:map_northern_edge-dx/2.0];
X=XX/1000.0;
Y=YY/1000.0;
nx=length(XX);
ny=length(YY);

inset_rect = [-199, -2249, 31, 21];

load Catania_surface.mat;
srf = map_data;

load Catania_vx.mat;
vx = map_data;

load Catania_vy.mat;
vy = map_data;

v = (vx.^2 + vy.^2).^0.5;


%%  GPS
% order stns in same order as organize_2011_velo_data.m script
stnlist={'19N1', '22N4', '25N1', '28N4', '33N1', '37N4', '38S3', '41N1', 'FOXX', 'GULL', 'HARE'};

gpswinter=[107.5, 76,     77,    68,      85,     84,      141,   118,     83.5,  77,     109];
% ,69.43818,-49.92776,625,19N1
% ,69.47387,-49.87859,730,22N4
% ,69.44550,-49.78465,836,25N0
% ,69.48642,-49.74294,876,28N4
% ,69.47051,-49.64036,927,33N1
% ,69.52118,-49.60509,988,37N4
% ,69.48456,-49.47901,1006,38S3
% ,69.51898,-49.47063,1039,41N0
% FOX,69.44621,-49.88188,667,22N1 
% GULL,69.45238,-49.71550,852,28N0
% HARE,69.49313,-49.54708,993,37N0

% QING,69.57268,-50.05064,654,XXXX


lat=[
69.43818
69.47387
69.4455
69.48642
69.47051
69.52118
69.48456
69.51898
69.44621
69.45238
69.49313
];

lon=[
-49.92776
-49.87859
-49.78465
-49.74294
-49.64036
-49.60509
-49.47901
-49.47063
-49.88188
-49.7155
-49.54708
];

elev=[
625
730
836
876
927
988
1006
1039
667
852
993
];




%% old ordering
% FOX,69.44621,-49.88188,667,22N1
% ,69.51898,-49.47063,1039,41N0
% HARE,69.49313,-49.54708,993,37N0
% ,69.47051,-49.64036,927,33N1
% ,69.48642,-49.74294,876,28N4
% ,69.47387,-49.87859,730,22N4
% ,69.52118,-49.60509,988,37N4
% ,69.48456,-49.47901,1006,38S3
% GULL,69.45238,-49.71550,852,28N0
% ,69.43818,-49.92776,625,19N1
% ,69.44550,-49.78465,836,25N0
% QING,69.57268,-50.05064,654,XXXX


% lat=[
%     69.44621
% 69.51898
% 69.49313
% 69.47051
% 69.48642
% 69.47387
% 69.52118
% 69.48456
% 69.45238
% 69.43818
% 69.4455
% 69.57268
% ];
% 
% lon=[
%     -49.88188
% -49.47063
% -49.54708
% -49.64036
% -49.74294
% -49.87859
% -49.60509
% -49.47901
% -49.7155
% -49.92776
% -49.78465
% -50.05064
% ];
% 
% elev=[
%     667
% 1039
% 993
% 927
% 876
% 730
% 988
% 1006
% 852
% 625
% 836
% 654
% ];
%     
    

%% convert to polar stereographic proj coords
%[X,Y]=POLARSTEREO_FWD(LAT,LONG,EARTHRADIUS,ECCENTRICITY,LAT_TRUE,LON_POSY) 

[gpsx, gpsy] = polarstereo_fwd(lat, lon, 6378137, 0.0818191908426215, 70.0, -45.0);


[qingx, qingy] = polarstereo_fwd(69.57268, -50.05064, 6378137, 0.0818191908426215, 70.0, -45.0);

%% plot it

figure(1); clf;

subplot(2,2,1); hold all;
imagesc(X, Y, flipud(bed))
colorbar
title('bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')

subplot(2,2,2); hold all;
imagesc(X, Y, flipud(srf))
colorbar
title('srf')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
%caxis([min(elev), max(elev)])

subplot(2,2,3); hold all;
imagesc(X, Y, flipud(srf-bed))
colorbar
title('H')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')

% subplot(2,2,3); hold all;
% imagesc(X, Y, vx)
% colorbar
% title('vx')
% axis equal
% plot(gpsx/1000.0, gpsy/1000.0, 'xk')

subplot(2,2,4); hold all;
imagesc(X, Y, flipud(v))
colorbar
title('v')
axis equal
caxis([0 180])
plot(gpsx/1000.0, gpsy/1000.0, 'xk')

rectangle('Position', inset_rect)

colormap('jet')


%% high res surface elevation GIMP

gimp = imread('/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/dem/gimp90-rogue.tif');
%x,y of  upper left corner pixel center:
%-216325.000000000000000,-2212145.000000000000000, 90m res
% 1109x756
nxgimp=1109; nygimp=756;
gimpx=[-216325.000000000000000:90:-216325.000000000+90.0*(nxgimp-1)];  % cell center x
gimpy=[-2212145.000000:-90:-2212145.000000-90*(nygimp-1)];  % cell center y
gimpy=fliplr(gimpy);

figure(2222); clf; % ORIG 90m
subplot (2,1,1); hold all
imagesc(gimpx/1000, gimpy/1000, flipud(gimp))
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
axis equal
plot(qingx/1000, qingy/1000, '*k')
caxis([100 1600])

% average onto the 500m grid we are using
gimp500=srf*0;
for i=1:nx
    for j=1:ny
        indx = find( abs(gimpx-XX(i))<250.0);
        indy = find( abs(gimpy-YY(ny+1-j))<250.0);
        
        if (~isempty(indx) && ~isempty(indy)) 
            here=gimp(nygimp+1-indy, indx);
            srf(j,i);
           gimp500(j,i) = mean(mean(here));
        end
    end
end

subplot (2,1,2); hold all  % RESAMPLED 500
imagesc(X, Y, flipud(gimp500))
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
axis equal
plot(qingx/1000, qingy/1000, '*k')
caxis([100 1600])

%% compare jesse bed to gimp bed

figure(1111); clf; 
subplot(2,1,1) ; hold all
[grx gry]=gradient(srf,500,500);
slope=(grx.^2+gry.^2).^0.5;
imagesc(X, Y, flipud(slope)); caxis([0 .04])
%imagesc(X, Y, flipud(srf)); caxis([100 1100])
colorbar
plot(gpsx/1000.0, gpsy/1000.0, 'ok')
colormap('jet')
axis equal
plot(qingx/1000, qingy/1000, '*k')

subplot(2,1,2) ; hold all
[gimpgrx gimpgry]=gradient(gimp500,500,500);
gimpslope=(gimpgrx.^2+gimpgry.^2).^0.5;
imagesc(X, Y, flipud(gimpslope)); caxis([0 .04])
%imagesc(X, Y, flipud(gimp500)); caxis([100 1100])

colorbar
plot(gpsx/1000.0, gpsy/1000.0, 'ok')
colormap('jet')
axis equal
plot(qingx/1000, qingy/1000, '*k')


% =============================
% USE GIMP SURFACE!!!!!
srf=gimp500;
% =============================



%% look at bed topo - break into regions

figure(222); clf; hold all

ax1=subplot(2,2,1); hold all;
imagesc(X, Y, flipud(bed))
colorbar
title('bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
caxis   ([0 500])
%caxis   ([250 251])



%curv=del2(bed);
% my own del2 operator
c=3; % how far in each direction to look
curv = (circshift(bed,[0 c]) + circshift(bed,[0 -c]) + circshift(bed,[c 0]) + circshift(bed,[-c 0]))/4.0 - bed;

extrema=curv*0;
c=5;
for j=c+1:nx-c
    for i=c+1:ny-c
        local=curv(i-c:i+c, j-c:j+c);
        if max(local(:)) == curv(i,j) && curv(i,j)>0
            extrema(i,j) = 1;
        end
        if min(local(:)) == curv(i,j) && curv(i,j)<0
            extrema(i,j) = -1;
        end
        
    end
end


ax2=subplot(2,2,2); hold all;
imagesc(X, Y, flipud(curv))
colorbar
title('del2 bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xr')
caxis   ([-1 1]*1)

ax3=subplot(2,2,4); hold all;
imagesc(X, Y, flipud(extrema))
colorbar
title('local extrema')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xr')

linkaxes([ax1 ax2 ax3])


%% make patches


valleylist=find(extrema==1);
ridgelist=find(extrema==-1);

% flood fill
basins=curv*0;
% seed flood fill
for rr=1:length(valleylist)
    r=valleylist(rr);
    [ii,jj]=ind2sub(size(basins), r);
    basins(ii,jj) = rr;
end

for iter=1:10;  % assume only need <10 iterations
   for rr=1:length(valleylist)
       r=valleylist(rr);
       members=find(basins==rr);
       for m=1:length(members)
          [ii,jj]=ind2sub(size(basins), members(m));
          for i=[-1,0,1]
              for j=[-1,0,1]
                if ii+i<ny && ii+i>0  && jj+j<nx && jj+j>0
                if curv(ii+i,jj+j)>0 && basins(ii+i,jj+j)==0
                    basins(ii+i,jj+j) = rr;
                end
                end
              end
          end
                    
          
       end
   end
end
subplot(2,2,3); hold all;
imagesc(X, Y, flipud(basins))
colorbar
title('basins')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xr')
C(:,1)=rand(length(valleylist),1);
C(:,2)=rand(length(valleylist),1);
C(:,3)=rand(length(valleylist),1);
colormap(C)


%% Mean InSAR data
fname='/Users/mhoffman/documents/greenland_geometry/MEASURES/processing/mean_insar_velo.nc';
x1=ncread(fname, 'x1');
y1=ncread(fname, 'y1');
insar=(ncread(fname, 'speed'));
insarx=(ncread(fname, 'insarx'));
insary=(ncread(fname, 'insary'));

offsetx=0;
offsety=0;
i1=find(x1>map_western_edge,1)+offsetx;
i2=find(x1<map_eastern_edge,1,'last')+offsetx;
j1=find(y1>map_southern_edge,1)+offsety;
j2=find(y1<map_northern_edge,1,'last')+offsety;

insar_clip=flipud(insar([i1:i2], [j1+1:j2]+1)');
insar_clipx=flipud(insarx([i1:i2], [j1+1:j2]+1)');
insar_clipy=flipud(insary([i1:i2], [j1+1:j2]+1)');


figure(123); clf; 
%imagesc(insar')
crange=[50 150];
crange=[0 200];

ax1=subplot(1,3,1); hold all
imagesc(X,Y,flipud(insar_clip))
axis equal
colorbar
caxis(crange)
title('insar mean of 6 years')

ax2=subplot(1,3,2) ; hold all
imagesc(X,Y,flipud(v))
axis equal
colorbar
caxis(crange)
title('Jesses insar dataset')

ax3=subplot(1,3,3) ; hold all
imagesc(X,Y,(flipud(v)-flipud(insar_clip)))
axis equal
colorbar
caxis([-1 1]*10)
title('diff')

linkaxes([ax1 ax2 ax3])

plot(gpsx/1000, gpsy/1000, 'xk')



%% compare GPS velo to insar

figure(777); clf;
subplot(1,2,1); hold all
vjunk=v;
for s=1:length(gpsx)
   [junk,i]=min(abs(XX-gpsx(s)));
   [junk,j]=min(abs((YY)-gpsy(s))) ;
   insargps(s)=v(ny-j,i);
   vjunk(ny-j,i) = 500.0;
   if s<5
       sym='o';
   elseif s<9
       sym='x';
   else
       sym='^';
   end
   plot(gpswinter(s), insargps(s), sym,  'Displayname',stnlist{s})
end
legend show
plot([0 1]*150, [0 1]*150,'k')
xlabel('gps winter speed')
ylabel('jesse insar speed')
axis equal

subplot(1,2,2); hold all
vjunk=flipud(insar_clip');
for s=1:length(gpsx)
   [junk,i]=min(abs(XX-gpsx(s)));
   [junk,j]=min(abs((YY)-gpsy(s))) ;
   myinsargps(s)=vjunk(ny-j,i);
   vjunk(ny-j,i) = 500.0;
   if s<5
       sym='o';
   elseif s<9
       sym='x';
   else
       sym='^';
   end
   plot(gpswinter(s), myinsargps(s), sym,  'Displayname',stnlist{s})
end
legend show
plot([0 1]*150, [0 1]*150,'k')
xlabel('gps winter speed')
ylabel('my insar speed')
axis equal

figure(555); clf;
imagesc(vjunk)
colorbar
caxis([40 180])


%% STICK mean INSAR INTO VELO?

figure(654); clf;
subplot(1,3,1) 
imagesc(vx)

v=insar_clip';
vx=flipud(insarx([i1:i2], [j1+1:j2]+1)');
vy=flipud(insary([i1:i2], [j1+1:j2]+1)');

subplot(1,2,2) 
imagesc(vx)

%% surface slope


figure(56); clf; hold all

imagesc(X, Y, gradient(srf))
colorbar
caxis([-20 20])
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
colormap('jet')
%% find cells where GPS live

% arrays for the cell indices
gpsxind = gpsx*0.0;
gpsyind = gpsy*0.0;
maskgps = bed * 0.0;

for g = 1:length(gpsx)
   gpsxind(g) = find( abs(gpsx(g) - XX) < (0.5 * dx) );
   gpsyind(g) = ny - find( abs(gpsy(g) - YY) < (0.5 * dx) ) + 1;   
   maskgps(gpsyind(g), gpsxind(g)) = 1.0;

   % to make 3x3 blocks instead of single cells:
   maskgps(gpsyind(g)-1:gpsyind(g)+1, gpsxind(g)-1:gpsxind(g)+1) = 1.0;
   
end

figure(2); clf;

subplot(1,2,1); hold all;
imagesc(X, Y, flipud(bed))
colorbar
title('bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')

subplot(1,2,2); hold all;
imagesc(X, Y, flipud(maskgps))
colorbar
title('GPS cells')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')

rectangle('Position', inset_rect)


%% insert GPS data?

% pasted this in from other script
day188velo=[-170.827571479208,-50.3346984367545;-134.714365935124,-51.8345091260653;-159.244495152789,-50.7525608593187;-129.105568691812,-44.1282873547698;-164.025260801182,-139.344905901908;-160.761995699540,-75.6423647189740;-225.155044702951,-268.663850456778;-202.808581713525,-186.208327884729;-143.561528908999,-70.1305154986403;-191.660035835603,-70.2804825286148;-204.800169541395,-199.312911887027];
day150velo=[[-102.798792261233,-31.0210846762948;-69.0923285083318,-28.2301234338411;-72.1716713492788,-21.6524288743494;-64.7897885021484,-21.9648116782173;-66.1640660341800,-53.1894707841821;-75.3808384718468,-36.1957001438395;-88.3730538034433,-109.837001939641;-87.7276191816782,-79.5131526661180;-76.1549078924827,-34.7508329641732;-70.2024150488705,-29.6861682011877;-73.9497750787086,-79.6898274163959]];
day180velo=[-146.931865280735,-44.3395536277785;-103.160609024239,-49.5690531174023;-108.667172662936,-33.0042758766831;-103.892655200052,-37.4925381206140;-98.6167329126783,-76.2266697019004;-107.941583061693,-49.5527802031938;-110.818999767653,-141.792969448861;-107.816171326795,-100.512470652516;-107.103030356270,-48.1012060931425;-110.399808864704,-51.7177377305052;-97.6487010526398,-107.207684529640];
day267velo=[-84.6899107678974,-25.9869797869396;-60.1878662985610,-24.9355516429387;-63.3984025423979,-19.1324128532233;NaN,NaN;-58.7866782378387,-46.6749072729726;-68.7469261691513,-33.0679544085524;-79.1897156309824,-100.587821635791;-79.1641719734980,-72.3487348116487;NaN,NaN;-60.1281862809810,-25.9398402642455;-67.4060328551968,-73.2552845176178];
% replace NaNs by day150*0.885
day267velo(isnan(day267velo)) = day150velo(isnan(day267velo))*0.885;
% time 182.7 - middle of a lake drainage inside the network
day182_7velo=[-226.499992608810,-81.4229882346353;-176.327045243661,-75.4856690069028;-143.782707502851,-43.3557608280898;-149.720795878866,-47.6970153092498;-127.307881091338,-91.5900140148642;-112.294522360971,-53.3934531042587;-117.498156171241,-147.140346511696;-115.393026397142,-106.119339588854;-185.756686113441,-99.5480179671995;-143.781433683916,-62.3181482541077;-107.958393335866,-115.802077502167];
day182_6velo=[-253.688341923062,-113.577245302295;-349.568205540545,-116.208801338689;-185.485091689297,-58.9019763808392;-223.671913028629,-71.6738980335504;-139.580258449787,-59.9798417008424;-118.620287516678,-58.7092970610502;-119.360272284175,-146.180269421020;-115.435508274968,-108.046793054637;-275.387024983211,-221.313288853526;-220.263508332973,-78.7642625023727;-117.365703336473,-123.644845710405];
day181_7velo=[-144.460682984504,-44.4580736725402;-113.756223177552,-50.1692119873496;-115.983229997537,-36.0299331752321;-123.616441667637,-38.0610688145589;-118.507401558973,-90.4806547577664;-107.418266899804,-50.9652557517848;-114.013137672391,-141.830083781424;-111.215335301476,-102.647131335793;-111.057508682975,-53.8790196089212;-120.918277185228,-56.4221018949047;-101.097584662748,-107.642752719530];
day181_2velo=[-145.863993135440,-43.9999199365684;-103.674809121212,-48.4400751753674;-110.043127577801,-33.2826897516043;-103.603595012155,-36.9231163447785;-98.7998728878847,-76.4003045063873;-106.321711797591,-50.0070404531380;-111.631720437435,-141.918159366360;-108.403111589487,-101.248262508016;-107.360264508056,-48.4375841309884;-111.398000751824,-51.1535488268146;-98.1278619885779,-107.184615367387];
day182_2velo=[-180.600507567610,-63.4610645259909;-154.999087649476,-64.5402188244680;-132.940521071614,-41.7358305033304;-160.820049705626,-45.5550190998656;-145.468189954545,-106.037404696654;-111.098149644095,-52.9228516103058;-116.627207648326,-144.239320380148;-114.090299571981,-105.278109631599;-149.388860695203,-82.2449522060924;-142.353859120076,-66.8937084357094;-106.579760928162,-112.585820474815];
day184_2velo=[-127.953816620261,-34.8947382663937;-79.1351861079602,-38.8931812050851;-94.1034561358265,-27.0440369913740;-76.4910953917206,-29.3792577407930;-83.3554152473698,-69.4828142837105;-106.918733020630,-49.9938196323365;-111.478597336463,-146.522085070774;-117.176351457601,-109.775102724948;-87.6283395042674,-33.5368699005176;-88.3668491983735,-36.8106610944687;-96.4306492501055,-108.440705107041];
day183_2velo=[-198.936745780966,-60.5063599700497;-126.659354699973,-59.6731170754356;-124.671239487870,-35.8192597562083;-97.9221670611332,-37.4933589758530;-90.3226107897899,-71.1666786090882;-108.546633934238,-50.8702425486126;-114.699064618690,-146.658206527391;-114.542713552273,-105.015914991059;-147.300456490279,-64.9377445829850;-112.619044728644,-44.9052347220004;-101.375579702690,-111.041875379769];
day183_7velo=[-146.282930459852,-37.1913324506295;-83.9301772520184,-41.4362660624738;-102.100853946604,-28.8946669582856;-74.9174003473218,-30.0087801581503;-80.5656945409828,-67.8037126623349;-106.149768351024,-49.5481215916425;-112.011861263491,-146.218325844638;-115.121560254120,-107.312313069292;-98.4928564906386,-35.3794743515573;-91.1248628623707,-36.6310708091351;-96.2599441705424,-107.336839861473];
dayVeloToUse = day181_7velo;
%=======
load('gps_velo.mat'); % loads 'data' from this file
dayVeloToUse = data;
%=======



% optional - stick insar data as synthetic gps data for cross-validation
%for g = 1:length(gpsx)
%   dayVeloToUse(g, 1) = vx(gpsyind(g), gpsxind(g));
%   dayVeloToUse(g, 2) = vy(gpsyind(g), gpsxind(g));
%end


figure(3); clf;

ax(1)=subplot(2,2,1); hold all;
imagesc(X, Y, flipud(vx))
colorbar
title('vx')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vxrng=[-300, 100];
caxis(vxrng)


ax(2)=subplot(2,2,2); hold all;
imagesc(X, Y, flipud(vy))
colorbar
title('vy')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vyrng=[-300, 100];
caxis(vyrng)

%----

%vx(:,:)=0.0; vy(:,:)=0.0;
for g = 1:length(gpsx)
    
    vx(gpsyind(g), gpsxind(g)) = dayVeloToUse(g, 1);
    vy(gpsyind(g), gpsxind(g)) = dayVeloToUse(g, 2);
    
   % to make 3x3 blocks instead of single cells:
   vx(gpsyind(g)-1:gpsyind(g)+1, gpsxind(g)-1:gpsxind(g)+1) = dayVeloToUse(g, 1);
   vy(gpsyind(g)-1:gpsyind(g)+1, gpsxind(g)-1:gpsxind(g)+1) = dayVeloToUse(g, 2);
% 
%    % to make 5x5 blocks instead of single cells:
%    vx(gpsyind(g)-2:gpsyind(g)+2, gpsxind(g)-2:gpsxind(g)+2) = dayVeloToUse(g, 1);
%    vy(gpsyind(g)-2:gpsyind(g)+2, gpsxind(g)-2:gpsxind(g)+2) = dayVeloToUse(g, 2);
   
end

%----

ax(3)=subplot(2,2,3); hold all;
imagesc(X, Y, flipud(vx))
colorbar
title('vx w/gps')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
caxis(vxrng)

ax(4)=subplot(2,2,4); hold all;
imagesc(X, Y, flipud(vy))
colorbar
title('vy w/gps')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
caxis(vyrng)

rectangle('Position', inset_rect)


linkaxes(ax)


%% Calculate IDW weights


% calculate 2d coord matrices
[Xmesh,Ymesh] = meshgrid(XX,YY);

figure (4) ; clf; hold all

gdist = zeros([length(gpsx), ny, nx]);  % distance to this gps station from all cells
wt = zeros([length(gpsx), ny, nx]);  % IDW weight to this GPS for each cell
for g = 1:length(gpsx)
   gdist(g,:,:) = ( (gpsx(g) - Xmesh).^2 + (gpsy(g) - Ymesh).^2 ).^0.5;
   wt(g,:,:) = gdist(g,:,:).^-3;    %  <-- set power of IDW here!!!
   subplot(3,4,g); hold all;
   imagesc(X,Y, squeeze(wt(g,:,:))) 
   colorbar
   axis equal
end

% zero out all but the nearest n stations
n=3;
for j=1:ny
    for i=1:nx
        [ASorted AIdx] = sort(squeeze(gdist(:,j,i)));
        smallestNElements = ASorted(1:n);
        smallestNIdx = AIdx(1:n);
        smallestMask = zeros(length(ASorted),1);  smallestMask(smallestNIdx)=1;
        wt(~smallestMask,j,i)=0.0;  % zero out these weights
    end
end


mingdist = squeeze( min(gdist, [], 1));  % distance to nearest GPS station for each cell (m)

figure(66); clf; hold all
imagesc(X,Y, mingdist/1000.0)
colorbar
title('min dist to a gps point, km')

%% insert GPS data everywhere using interpolation and errors
figure(5); clf;

ax(1)=subplot(2,2,1); hold all;
imagesc(X, Y, flipud(vx))
colorbar
title('vx')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vxrng=[-300, 100];
caxis(vxrng)


ax(2)=subplot(2,2,2); hold all;
imagesc(X, Y, flipud(vy))
colorbar
title('vy')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vyrng=[-300, 100];
caxis(vyrng)

% need to check for nans eventually
sumwt = squeeze(sum(wt, 1));  % sum of all weights (after only some have been kept)
vx = vx*0.0; vy = vy*0.0;
ex = vx*0.0; ey = vy*0.0;

for g = 1:length(gpsx)
  vx = vx + dayVeloToUse(g, 1) * squeeze(wt(g, :, :));
  vy = vy + dayVeloToUse(g, 2) * squeeze(wt(g, :, :));  
end

vx = vx ./ sumwt;
vy = vy ./ sumwt;



%ex = mingdist/1000.0/30.0*70.0 * 6 + 1;
%ey = mingdist/1000.0/30.0*70.0 * 6 + 1;

% set error as empirical function based on semivariogram analysis
meanNetworkSpeed = mean((dayVeloToUse(:, 1).^2 + dayVeloToUse(g, 2).^2).^0.5);
normalizederror2d = 0.35 * (mingdist/1000.0-0.55).^0.5;  %  <-- from organize_2011_velo_data.m
normalizederror2d(mingdist<550.0) = 0.0;
errorSpeed2d = normalizederror2d * meanNetworkSpeed;
ex = errorSpeed2d/2^0.5 + 1.0;
ey=ex;

ax(3)=subplot(2,2,3); hold all;
imagesc(X, Y, (vx))
colorbar
title('vx interp')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vxrng=[-300, 100];
caxis(vxrng)


ax(4)=subplot(2,2,4); hold all;
imagesc(X, Y, (vy))
colorbar
title('vy interp')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
vyrng=[-300, 100];
caxis(vyrng)


linkaxes(ax)


%% try using DT barycentric interp

% Fx = scatteredInterpolant([gpsx, gpsy],dayVeloToUse(:,1), 'natural', 'linear');
% Fy = scatteredInterpolant([gpsx, gpsy],dayVeloToUse(:,2), 'natural', 'linear');
% 
% vx = Fx(Xmesh, Ymesh);
% vy = Fy(Xmesh, Ymesh);
% 
% 
% ax(3)=subplot(2,2,3); hold all;
% imagesc(X, Y, (vx))
% colorbar
% title('vx interp')
% axis equal
% plot(gpsx/1000.0, gpsy/1000.0, 'xk')
% vxrng=[1.5*max(dayVeloToUse(:,1)), 0.5*max(dayVeloToUse(:,1))];
% caxis(vxrng)
% 
% 
% ax(4)=subplot(2,2,4); hold all;
% imagesc(X, Y, (vy))
% colorbar
% title('vy interp')
% axis equal
% plot(gpsx/1000.0, gpsy/1000.0, 'xk')
% vyrng=[1.2*max(dayVeloToUse(:,2)), 0.8*max(dayVeloToUse(:,2))];
% caxis(vyrng)



%% zero some thickness
DT = delaunayTriangulation([gpsx, gpsy]);
K = convexHull(DT);
IN = inpolygon(Xmesh, Ymesh, gpsx(K),gpsy(K));

IN2=IN;

INtemp=IN2;

for s=1:8;  % number of times to grow region
    
    INtemp=IN;
    for sx=[1,0,-1]
        for sy=[1,0,-1]
           IN2 = IN2 + circshift(INtemp, [sx sy]);
        end
    end

end
ind = flipud(IN2)==0;


% get a convex hull for a X km radius around the GPS stn
gpsxBuffer = [];
gpsyBuffer = [];
r = 4000.0 ;  %   <------- the buffer radius for the domain

for s = 1:length(gpsx);
    for a=0:0.05:2*pi
       gpsxBuffer = [gpsxBuffer; gpsx(s) + r * cos(a)];
       gpsyBuffer = [gpsyBuffer; gpsy(s) + r * sin(a)];       
    end
end
DT = delaunayTriangulation([gpsxBuffer, gpsyBuffer]);
K = convexHull(DT);
INbuffer = inpolygon(Xmesh, Ymesh, gpsxBuffer(K),gpsyBuffer(K));
ind = (flipud(INbuffer)==0);   
    



% save convex hull as vtk points
path3d = DT.Points(K,:);
path3d=cat(2, path3d, zeros([length(path3d) 1]));
path3d=path3d/1000.0;  % convert to km
export3Dline2VTK('convexhull',path3d,'/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson/albany_mesh/exodus/inversion')


 


figure(44); clf; hold all
plot(gpsxBuffer, gpsyBuffer, 'o')
plot(gpsx, gpsy, '*')

%ind = ~((flipud(mingdist) < 2000.0) | (IN==0));  % within 2km or convex hull


%maskkeep = (flipud(mingdist) < 2500.0) | (flipud(IN)==1) ;
%ind=(maskkeep==0);
%ind = (flipud(mingdist) > 4000.0);

% 0 thickness outside of this region
srf(ind)=bed(ind);

% =========================
% increase RMS outside of convex hull
%ex(IN==0) = ex(IN==0)*2.0;
%ey=ex;
% =========================


% build a mask for within a certain distance to boundary to use to increase
% RMS in INSAR runs to downweight edge regions.
gpsxBuffer = [];
gpsyBuffer = [];
r = 2000.0; %   <-------  buffer radius around GPS beyond which has increased RMS
for s = 1:length(gpsx);
    for a=0:0.05:2*pi
       gpsxBuffer = [gpsxBuffer; gpsx(s) + r * cos(a)];
       gpsyBuffer = [gpsyBuffer; gpsy(s) + r * sin(a)];       
    end
end
DT = delaunayTriangulation([gpsxBuffer, gpsyBuffer]);
K = convexHull(DT);
INbufferINSARcenter = inpolygon(Xmesh, Ymesh, gpsxBuffer(K),gpsyBuffer(K));
  




%% slip insar back in 

% load Catania_vx.mat;
% vx = map_data;
% vx=flipud(vx);
% 
% load Catania_vy.mat;
% vy = map_data;
% vy=flipud(vy);
% 
% v = (vx.^2 + vy.^2).^0.5;
% 
% 
% 
% 
% ex=vx*0.0 + 5.0;
% 
% ind = ((INbufferINSARcenter)==0);
% ex(ind) = vx(ind)*0.0 + 40.0;
% 
% ey=ex;
% 
% 



%% define temp data


% # data from ~/documents/ROGUE/Models/local_mcb_domain/icetemps.xlsx
% for i in range(nx):
%    for j in range(ny):
%      flwastag[0,:,j,i] = (
% 6.24821E-17,
% 5.98918E-17,
% 3.78746E-17,
% 1.86614E-17,
% 9.50214E-18,
% 7.66194E-18,
% 7.9094E-18,
% 1.06029E-17,
% 3.31514E-17,
% 2.01455E-16)

Tcolumn=[-1.18, -1.41, -3.83, -7.49, -11.58, -13.61, -13.31, -10.54 , -4.52, -0.77, -0.5];



%% define flwa data


% build approximate flowline

% get indices
f=find(cellfun('length',regexp(stnlist,'FOXX')) == 1);
h=find(cellfun('length',regexp(stnlist,'HARE')) == 1);
g=find(cellfun('length',regexp(stnlist,'GULL')) == 1);

% get flowline eqn
p = polyfit([gpsx(h) gpsx(g)]/1000.0, [gpsy(h) gpsy(g)]/1000.0, 1);
mf = p(1);
bf = p(2);

% get eqn perp to flowline and through GULL
mp = -1.0/mf;
bp = gpsy(g)/1000.0 - mp * gpsx(g)/1000.0;


figure(22); clf;

subplot(1,2,1); hold all;
imagesc(X, Y, flipud(bed))
colorbar
title('bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
plot([-210:-160], mf*[-210:-160]+bf, 'r')
plot([-200:-170], mp*[-200:-170]+bp, 'y')


% calc dist to ortho line
P=[Xmesh(:) Ymesh(:)];  

Q1=[gpsx(g) gpsy(g)]/1000.0;  % first point on line is gull
Q2=[-200.0 mp*-200.0+bp];  % second point from eqn, choosing arbitraty location
clear dist2gull
dist2gull = zeros(size(bed));
for y=1:length(Y)
    for x=1:length(X)
        P=[X(x) Y(y)];  
%       dist2gull(y,x) = abs(det([Q2-Q1;P-Q1]))/norm(Q2-Q1);
       dist2gull(y,x) = -1.0*(mp*X(x) - 1.0*Y(y) + bp)/norm([mp, -1]);
    end
end


subplot(1,2,2); hold all;
imagesc(X, Y, (dist2gull))
colorbar
title('bed')
axis equal
plot(gpsx/1000.0, gpsy/1000.0, 'xk')
plot([-210:-160], mf*[-210:-160]+bf, 'r')
plot([-200:-180], mp*[-200:-180]+bp, 'y')
plot(Q1(1), Q1(2), '*r')
plot(Q2(1), Q2(2), '*m')

%%
% temp column
T_depth=[0,12,48,84,120,156,192,228,264,300,307,407,497,537,577,622,667,687,697,702,705,707];
T_value=[-1.45000000000000,-1.45000000000000,-1.04000000000000,-0.850000000000000,-1.76000000000000,-2.88000000000000,-4.53000000000000,-6.43000000000000,-8.40000000000000,-10.1800000000000,-11.2600000000000,-14.1300000000000,-12.7300000000000,-10.0900000000000,-6.55000000000000,-2.73000000000000,-0.830000000000000,-0.570000000000000,-0.530000000000000,-0.470000000000000,-0.400000000000000,-0.500000000000000];
T_sigma=T_depth/T_depth(end);

nz=10;
nzint=nz+1;
sigmaint=[0:1.0/nz:1.0];

level = [0:1.0/(nz-1):1.0];
staglevel=zeros([nz-1,1]);
for z=1:nz-1
    staglevel(z) = (level(z)+level(z+1))/2.0;
end

T_value_interp = interp1(T_sigma, T_value, staglevel);

figure(88); clf;
hold all;
plot(T_value, -1*T_sigma, 'o-')
plot(T_value_interp, -1*staglevel, 'x-')


% build 3d temp field
thktmp=flipud(srf-bed);

temper = zeros([nz-1,ny,nx]);

%flwa_stag is the desired field.  vertical dimension is staglevel (w/o boundaries)
% dimensions:    time, staglevel, y1, x1
% units:         pascal**(-n) year**(-1)
flwastag = zeros([nx,ny,nz-1]);
n=3;
for y=1:length(Y)
    for x=1:length(X)
        % this is an approximation of the how the profile changes based on
        % Luethi TC 2015 Fig 2b, using GULL profile as the baseline
        temper(:, y,x) = T_value_interp * (dist2gull(y,x)*0.055+1.0);
        % calculate flwa
        for z=1:nz-1
            Tk = temper(z, y,x) +273.15;
            P=910*9.81*sigmaint(z)*thktmp(j,i);
            Tstar = 263.0 + 7.0e-8 * P;
            Th = Tk +  7.0e-8 * P;
            if (Th<Tstar)
                Qc=60.0e3;
            else
                Qc=115.0e3;
            end
            flwastag(x,y,z) = 3.5e-25 * exp(-Qc/8.314*(1.0/Th - 1.0/Tstar));

        end
    end
end



%% plot 3d temp
figure (5436); clf; hold all
%[x3,y3,z3] = meshgrid(X,Y,1-staglevel);
%hz=slice(x3,y3,z3,permute(temper, [2 3 1]),[-215, -156], [-2266, -2203], [.5]);
[x3,y3,z3] = meshgrid(X,Y,1-staglevel);
hz=slice(x3,y3,z3,log10(permute(flwastag, [2 1 3 ])),[-215, -156], [-2266, -2203], [.5]);

for i=1:5
  hz(i).FaceColor='interp';
  hz(i).EdgeColor='k';
end
colorbar
%lightangle(-45,70)
%set(gca, 'CameraViewAngle', 10.304);

%% add enhancement factor
 flwastag(:,:,end) = flwastag(:,:,end) * 1.0;  % depth=94%
 flwastag(:,:,end-1) = flwastag(:,:,end-1) * 1.0;  % depth=83%
 flwastag(:,:,end-2) = flwastag(:,:,end-2) * 1.0;  % depth=72%
 % convert to CISM units
 flwastag = flwastag*(365*24*3600);

% get indices to locations of interest.
[junk,ig] = min( abs(X - gpsx(g)/1000.0));
[junk,jg] = min( abs(Y - gpsy(g)/1000.0));

[junk,iif] = min( abs(X - gpsx(f)/1000.0));
[junk,jf] = min( abs(Y - gpsy(f)/1000.0));

[junk,ih] = min( abs(X - gpsx(h)/1000.0));
[junk,jh] = min( abs(Y - gpsy(h)/1000.0));


plot(temper(:,j,i), -1*staglevel, '^')

plot(temper(:,j,i), -1*staglevel, 's')

plot(T_value_interp * (-20*-0.055+1.0), -1*staglevel, '.')


%% plot with actual depth
figure(89); clf; 
subplot(1,1,1), hold all;
%ww=12.5; hh=9.5;
%set(gcf, 'Units', 'Inches', 'Position', [0, 0, ww, hh], 'PaperUnits', 'Inches', 'PaperSize', [ww hh])
set(gcf,'color','w');

plot(temper(:,jf,iif), -1*staglevel*(thktmp(jf,iif)), '*-b')

plot(temper(:,jg,ig), -1*staglevel*(thktmp(jg,ig)), '^-r')

% approximate TD5/Swiss Camp location...
x=(gpsx(g)/1000.0+20.0);
y=mf*x + bf;
[junk,i] = min( abs(X - gpsx(h)/1000.0));
[junk,j] = min( abs(Y - gpsy(h)/1000.0));
plot(temper(:,j,i), -1*staglevel*(thktmp(j,i)), '*-y')
%set(gca,'FontSize',12)
xlabel('Temperature (deg. C)')
ylabel('Depth (m)')
legend('FOX', 'GULL', 'TD5', 'location', 'northwest')
box on

print('borehole_temps', '-dpdf')


% this plot helps make sure indices are correct.
% figure(90); clf;
% hold all
% pcolor(thktmp); colorbar
% 
% plot(iif,jf,'r*')
% plot(ig,jg,'r*')


%% Define subset region

% for now don't bother implementing this below.  Just use ncks to cut out
% the domain after the fact - less confusing.

xs = find(X>=-199.0, 1);
xe = find(X>=(-199.0+31.0), 1);
ys = find(Y>=-2249.0, 1);
ye = find(Y>=(-2249.0+21.0), 1);

% xs = find(X>= min(gpsx)/1000.0-1, 1);
% xe = find(X>= max(gpsx)/1000.0+1, 1);
% ys = find(Y>= min(gpsy)/1000.0-1, 1);
% ye = find(Y>= max(gpsy)/1000.0+1, 1);


nxout = xe - xs + 1;
nyout = ye - ys + 1;

%% Create CISM style netCDF file


% dimensions and dimension variables

fcism = 'rogue_johnson_domain.cism.nc';
unix(['rm ' fcism]);

nccreate(fcism, 'x1', 'Dimensions', {'x1',nx}, 'Datatype', 'double')
nccreate(fcism, 'y1', 'Dimensions', {'y1',ny}, 'Datatype', 'double')
ncwrite(fcism, 'x1', XX)
ncwrite(fcism, 'y1', YY)

nccreate(fcism, 'x0', 'Dimensions', {'x0',nx-1}, 'Datatype', 'double')
nccreate(fcism, 'y0', 'Dimensions', {'y0',ny-1}, 'Datatype', 'double')
ncwrite(fcism, 'x0', XX(1:end-1)+dx/2)
ncwrite(fcism, 'y0', YY(1:end-1)+dx/2)

nz=10;
levs=([1:nz]-1.0) / (nz-1);
staglevs=(levs(1:end-1)+levs(2:end))/2.0;
stagwbndlevs = [levs(1), staglevs, levs(end)];
nccreate(fcism, 'level', 'Dimensions', {'level',nz}, 'Datatype', 'double')
ncwrite(fcism, 'level', levs)
nccreate(fcism, 'staglevel', 'Dimensions', {'staglevel',nz-1}, 'Datatype', 'double')
ncwrite(fcism, 'staglevel', staglevs)
nccreate(fcism, 'stagwbndlevel', 'Dimensions', {'stagwbndlevel',nz+1}, 'Datatype', 'double')
ncwrite(fcism, 'stagwbndlevel', stagwbndlevs)

nt = 1;
nccreate(fcism, 'time', 'Dimensions', {'time',nt}, 'Datatype', 'double')


% variables

% scalar grid
nccreate(fcism, 'topg', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data=fliplr(bed');
ncwrite(fcism, 'topg', data)

nccreate(fcism, 'topgerr', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data = fliplr(bed)' * 0.0;
ncwrite(fcism, 'topgerr', data)

nccreate(fcism, 'thk', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data = fliplr((srf-bed)');
ncwrite(fcism, 'thk', data)

nccreate(fcism, 'acab', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data = fliplr(bed') * 0.0;
ncwrite(fcism, 'acab', data)

nccreate(fcism, 'artm', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data = fliplr(bed') * 0.0;
ncwrite(fcism, 'artm', data)

nccreate(fcism, 'dhdt', 'Dimensions', {'x1',nx,'y1',ny,'time',nt}, 'Datatype', 'double')
data = fliplr(bed') * 0.0;
ncwrite(fcism, 'dhdt', data)

nccreate(fcism, 'tempstag', 'Dimensions', {'x1',nx,'y1',ny,'stagwbndlevel',nz+1,'time',1}, 'Datatype', 'double')
%Tall = shiftdim(repmat(Tcolumn', [1, nx, ny]),1);
ncwrite(fcism, 'tempstag', Tall)
%ncwrite(fcism, 'tempstag', temper)

nccreate(fcism, 'flwastag', 'Dimensions', {'x1',nx,'y1',ny,'staglevel',nz-1,'time',1}, 'Datatype', 'double')
ncwrite(fcism, 'flwastag', flwastag)

% velo grid
nccreate(fcism, 'beta', 'Dimensions', {'x0',nx-1,'y0',ny-1,'time',1}, 'Datatype', 'double')
ncwrite(fcism, 'beta', 1.0e5 * ones([nx-1,ny-1]))  % dummy value


%% VELOS


nccreate(fcism, 'vx', 'Dimensions', {'x1',nx, 'y1',ny, 'time',1}, 'Datatype', 'double')
%vxstag = (vx(1:end-1, 1:end-1) + vx(1:end-1, 2:end) + vx(2:end, 1:end-1) + vx(2:end, 2:end)) / 4.0;
%data = fliplr(vxstag');
%data = fliplr(vx');
data = ((vx'));
ncwrite(fcism, 'vx', data)

nccreate(fcism, 'vy', 'Dimensions', {'x1',nx, 'y1',ny, 'time',1}, 'Datatype', 'double')
%vystag = (vy(1:end-1, 1:end-1) + vy(1:end-1, 2:end) + vy(2:end, 1:end-1) + vy(2:end, 2:end)) / 4.0;
%data = fliplr(vystag');
%data = fliplr(vy');
data = ((vy'));
ncwrite(fcism, 'vy', data)

nccreate(fcism, 'velnorm', 'Dimensions', {'x1',nx, 'y1',ny, 'level', nz, 'time',1}, 'Datatype', 'double')
data = fliplr( (vx.^2 + vy.^2).^0.5' );
ncwrite(fcism, 'velnorm', data)

%% VELO ERRORS


%data = ones(size(data)) * 10.0;  % uniform value.
%goodval=1.0; data = maskgps * goodval; data(data==0) = 10000.0;      data = fliplr(data');  % first value at GPS stations + edges of domain; second value everywhere else.
%%data(1:3,:)=goodval; data(:,1:3)=goodval; data(end-2:end,:)=goodval; data(:,end-2:end)=goodval;

data = ex';
nccreate(fcism, 'ex', 'Dimensions', {'x1',nx, 'y1',ny,  'time',1}, 'Datatype', 'double')
ncwrite(fcism, 'ex', data)

data = ey';
nccreate(fcism, 'ey', 'Dimensions', {'x1',nx, 'y1',ny, 'time',1}, 'Datatype', 'double')
ncwrite(fcism, 'ey', data)


%% now cut out gps domain
fgpscism = 'rogue_gps_region.cism.nc';
system (['/opt/local/bin/ncks -O -F -d x1,' num2str(xs) ',' num2str(xe) ' -d y1,' num2str(ys) ',' num2str(ye) ' -d x0,' num2str(xs) ',' num2str(xe-1) ' -d y0,' num2str(ys) ',' num2str(ye-1) ' ' fcism ' ' fgpscism])



%% 


%% clean plot for paper: semivariogram, uncertainty, velo interp


figure (1234); clf; 
fontsize=10;
w=4.2; h=9;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])
set(gcf,'color','w');

ylimits=[-2.247e3 -2.229e3];
xlimits=[-198 -169];
xticks=[-200:5:-160];
yticks=[-2250:5:-2220];


% ==== semivariogram ====

subplot(3,1,1);hold all;
set(gca,'FontSize',fontsize)
box on

% zone of distance used
plot([1 1]*5.4810, [0 2], ':k');
patch([0 1 1 0]*5.4810, [0 0 1 1]*2, [1 1 1]*0.83, 'linestyle', 'none', 'facealpha', 0.5);

dayGPSSpeed_summer = (day181_7velo(:, 1).^2 + day181_7velo(:, 2).^2).^0.5;
dayGPSSpeed_lake = (day182_6velo(:, 1).^2 + day182_6velo(:, 2).^2).^0.5;
for i=1:length(dayGPSSpeed_summer)
    for j=1:length(dayGPSSpeed_summer)
        d=( (gpsx(i)-gpsx(j))^2 + (gpsy(i)-gpsy(j))^2)^0.5/1000.0;
        p1=plot(d, abs(dayGPSSpeed_summer(i)-dayGPSSpeed_summer(j))/mean(dayGPSSpeed_summer), 'b.', 'displayname', 'day 181.7');
        p2=plot(d, abs(dayGPSSpeed_lake(i)-dayGPSSpeed_lake(j))/mean(dayGPSSpeed_lake), 'g.', 'displayname', 'day 182.6');

    end
end

% the fit used
p3=plot([0:0.2:20], 0.35 * ([0:0.2:20]).^0.5, 'k-', 'displayname', 'f=0.35*d^{0.5}');  %  <-- from organize_2011_velo_data.m

legend([p1, p2, p3], 'location', 'northwest')

%plot([0:20], 20 * [0:20].^0.5, 'r-')
xlabel('distance, km')
ylabel('speed difference / network mean speed')
xlim([0 22])
ylim([0 2])


% === error function ===
ax1=subplot(3,1,2); hold all;
set(gca,'FontSize',fontsize)
box on 

imagesc(X,Y, ex,  'AlphaData', INbuffer)
plot(gpsx/1000.0, gpsy/1000.0, 'r.')
h = colorbar;
ylabel(h, '\sigma_u (m yr^{-1})')
caxis([0 85])

ylim(ylimits); xlim(xlimits);
ax1.XTick=xticks; ax1.YTick=yticks;
axis equal
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')
xlim(xlimits);

legend('GPS locations', 'location', 'northwest')
% === interpolated speed  ===
subplot(3,1,3); hold all;
set(gca,'FontSize',fontsize)
box on 

imagesc(X,Y, (vx.^2+vy.^2).^0.5,  'AlphaData', INbuffer)
plot(gpsx/1000.0, gpsy/1000.0, 'r.')
h = colorbar;
ylabel(h, 'speed (m yr^{-1})')

%caxis([0 80])
ylim(ylimits); xlim(xlimits);
ax1.XTick=xticks; ax1.YTick=yticks;
axis equal
xlim(xlimits);
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')


print('velo_uncertainty', '-dpdf')

