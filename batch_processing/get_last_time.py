#!/usr/bin/env python
'''
This script takes .exo output from a series of directories, each
containing a time slice, and does the following:
1. separate out the final time slice (iteration).
2. write the correct day of year into the time_slice variable
3. merge all time levels into a single file

Run this from the base of the directory tree you want merged


Needed tools:
1) Make sure you have Python v2.7 installed.
2) Download and install the SEACAS exodus version from https://github.com/gsjaardema/seacas.  You'll only need the exodus library, there's instructions on that page.
3) Download "exomerge.py" available at https://github.com/timkostka/exomerge and put it in the same directory as exodus.py.
4) Run python and try "import exomerge" and fix the errors that come up.  There should only be a few due to exodus.py using hardcoded paths.
'''

###################
conjoin = '/Users/mhoffman/software/trilinos/trilinos-install/bin/conjoin'
###################

import sys
#sys.path.append('/Users/mhoffman/software/trilinos/trilinos-install/lib/')
sys.path.append('/Users/mhoffman/software/seacas-exodus/seacas-exodus/install/lib')
sys.path.append('/Users/mhoffman/software/exomerge')
from exodus import exodus
import exomerge
import os
import shutil
import glob

outtypes =  ('surface', 'beta')



# get ordered list of subdirs
for outtype in outtypes:
      # extract the last day
      f = 'rogue_analysis_' + outtype + '.exo'
      # load the model the last timestep
      model = exomerge.import_model(f, timesteps='last')
      # save the model to a new file
      fout=f+'.finaliteration.exo'
      model.export_model(fout)


