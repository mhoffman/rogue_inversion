#!/bin/bash

# This script sets up the input dir structure and files to automate the GPS inversion
# source this script so you get env vars needed for albany
# RUN THIS FROM WHERE YOU WANT TO SET THINGS UP!
# This will create a dir structure like:
# ROOT
#    |-- meshdata
#    |-- dayDDD.dddd
#    |-- dayDDD.dddd


# base dir to repo that has the scripts in it
SCRIPTBASEDIR=/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson

# Matlab exe
MATLAB=/Applications/MATLAB_R2016a.app/bin/matlab

# Albany exe dir
ALBANYDIR=/Users/mhoffman/software/albany/albany-install/bin


mkdir meshdata
FIRST=true

#for i in {179..186}  # days to loop over
for i in {178..179}  # days to loop over
do
   #for j in {0..23}  # hrs per day to loop over
   for j in `seq 0 2 22`  # hrs per day to loop over
   do
       echo $i, $j
       TIME=`echo "scale = 4; $i+$j/24.0" | bc`
       echo $TIME

       # save the timestamp to work on - put it where the matlab script runs - this is used by the first matlab script to know what day to process
       echo $TIME>$SCRIPTBASEDIR/timestamp.txt

       # Run matlab scripts: 
       # 1) extract the gps data points for this time to a local file;   This gets the time slice from to use from $SCRIPTBASEDIR/timestamp.txt.  
       # 2) apply the extracted data into a netcdf file;  This gets the GPS data to use from gps_velo.mat written by the previous script
       # 3) create msh and .ascii files from netcdf
       # The matlab scripts run from their own directories!  (which function as temp dirs)
       $MATLAB -nodisplay -nodesktop -r "run $SCRIPTBASEDIR/organize_2011_velo_data.m; run $SCRIPTBASEDIR/setup_JJ_MCB_ROGUE_domain.m; run $SCRIPTBASEDIR/albany_mesh/build_cism_msh_from_nc.m; quit"

       # Now that we're done, copy needed files to the dir for this time
       mkdir day$TIME  # create subdir for this day
       cp $SCRIPTBASEDIR/gps_velo.mat day$TIME/
       cp $SCRIPTBASEDIR/albany_mesh/surface_velocity.ascii day$TIME/
       cp $SCRIPTBASEDIR/albany_mesh/velocity_RMS.ascii day$TIME/


       if [ $FIRST = "true" ]; then
            echo FIRST TIME SLICE - SETTING UP MESH INFO
            # do this on the first time slice so that we have this if we abort the script before completion
            # Now setup one copy of the mesh
            cd meshdata
            cp $SCRIPTBASEDIR/rogue_gps_region.cism.nc .
            cp $SCRIPTBASEDIR/rogue_johnson_domain.cism.nc .
            cp $SCRIPTBASEDIR/albany_mesh/*.ascii .  # get all the .ascii fields
            rm surface_velocity.ascii  # remove this to avoid confusion
            rm velocity_RMS.ascii  # remove this to avoid confusion
            cp $SCRIPTBASEDIR/albany_mesh/rogue_gps_region_Alb.quad.msh .  # get the .msh file
            cp $SCRIPTBASEDIR/albany_mesh/exodus/create2dExoForScript.xml .  # get the .xml file to create the mesh

            # now use the hacky Albany run to build the .exo
            $ALBANYDIR/AlbanyT create2dExoForScript.xml

            cd ..
            FIRST=false
       fi

   done
done


