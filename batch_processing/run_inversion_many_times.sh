#!/bin/bash

# This script runs Albany inversion for every subdir matching day* in a directory
# NOTE: Manually remove (or ignore) any time slices (dirs) you don't want included in the loop!
# source this script so you get env vars needed for albany
# RUN THIS FROM ROOT OF WHAT YOU WANT RUN.
# You must manually put the xml you want to use in the root dir with the name below!
# Keeping it there ensures all runs use the same settings and also that a copy of the settings
# used are kept with the runs for later reference.

ALBANYDIR=/Users/mhoffman/software/albany/albany-install/bin

date
for daydir in day*; do
#    [ -d "${path}" ] || continue # if not a directory, skip
    echo $daydir
    cd $daydir
    echo `pwd`
    time $ALBANYDIR/AlbanyAnalysis ../input_fo_gis_analysis_beta_forScript_flowfactor.xml &> albany.stdout.txt
    date
    cd ..
done
