clear all;

% load data from excel spreadsheets
summer1 = xlsread('~/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/178-186-reg0.5_nopressurestress_12-02-16_GIMP/LCURVETEST/L-curve.xlsx',...
    '178.2500','A3:C16');

summer2 = xlsread('~/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/178-186-reg0.5_nopressurestress_12-02-16_GIMP/LCURVETEST/L-curve.xlsx',...
   '178.9166','A3:C15');

lake = xlsread('~/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/178-186-reg0.5_nopressurestress_12-02-16_GIMP/LCURVETEST/L-curve.xlsx',...
    '182.5000','A3:C15');

winter = xlsread('~/documents/ROGUE/Models/gps_2011_inversion_workdir/insar_winter/L-curve.xlsx',...
     'insar-winter','A5:C17');

lcurve{1}.raw=winter;
lcurve{2}.raw=summer1;
lcurve{3}.raw=summer2;
lcurve{4}.raw=lake;


lcurve{1}.name = 'winter';
lcurve{2}.name = 'day 178.2500';
lcurve{3}.name = 'day 178.9166';
lcurve{4}.name = 'day 182.5000';



%% calculate stuff for each

for i=1:length(lcurve)
   lcurve{i}.alpha = lcurve{i}.raw(:,1);
   lcurve{i}.J0 = lcurve{i}.raw(:,2);
   lcurve{i}.reg = lcurve{i}.raw(:,3);
   lcurve{i}.Jreg = lcurve{i}.reg ./ lcurve{i}.alpha;  
   J0=lcurve{i}.J0;
   Jreg=lcurve{i}.Jreg;
   lcurve{i}.curvature = lcurve{i}.alpha * 0.0;
   for j=2:length(lcurve{i}.alpha)-1
     lcurve{i}.curvature(j) = 2*log(Jreg(j-1))/((log(J0(j))-log(J0(j-1))) * (log(J0(j+1))-log(J0(j-1)))) - ...
                              2*log(Jreg(j  ))/((log(J0(j+1))-log(J0(j))) * (log(J0(j  ))-log(J0(j-1)))) + ...
                              2*log(Jreg(j+1))/((log(J0(j+1))-log(J0(j))) * (log(J0(j+1))-log(J0(j-1))))
                              
%   =2*LN(F4)/((LN(E5)-LN(E4))*(LN(E6)-LN(E4)))-2*LN(F5)/((LN(E6)-LN(E5))*(LN(E5)-LN(E4)))+2*LN(F6)/((LN(E6)-LN(E5))*(LN(E6)-LN(E4)))

   end
end


lcurve{1}.chosenindex=5;
lcurve{2}.chosenindex=10;
lcurve{3}.chosenindex=10;
lcurve{4}.chosenindex=10;

lcurve{1}.thk=2;
lcurve{2}.thk=1;
lcurve{3}.thk=1;
lcurve{4}.thk=1;
%% figure

fontname = 'Helvetica';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

figure (101); clf;
w=10.5; h=4;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])
ax=subplot(1,1,1); hold all
set(gca,'FontSize',12)
set(gcf,'color','w');

subplot(121); 
for i=1:length(lcurve)
   h(i)=loglog(lcurve{i}.J0, lcurve{i}.Jreg, 'o-', 'displayname', lcurve{i}.name, 'linewidth', lcurve{i}.thk);
   n{i}=lcurve{i}.name;
   hold all;
   loglog(lcurve{i}.J0(lcurve{i}.chosenindex), lcurve{i}.Jreg(lcurve{i}.chosenindex), 'o', 'markersize',4, 'MarkerFaceColor','k','MarkerEdgeColor','none')

end
legend(h, n)
% plot the chosen values
%loglog(lcurve{1}.J0(6), lcurve{1}.Jreg(6), 'o', 'markersize',4, 'MarkerFaceColor','k','MarkerEdgeColor','none')
%loglog(lcurve{2}.J0(10), lcurve{2}.Jreg(10), 'o', 'markersize',4, 'MarkerFaceColor','k','MarkerEdgeColor','none')
xlabel('J')
ylabel('J_{reg}')

subplot(122); 
for i=1:length(lcurve)
   semilogx(lcurve{i}.alpha(2:end-1), lcurve{i}.curvature(2:end-1), 'o-', 'linewidth', lcurve{i}.thk)
   hold all;
   loglog(lcurve{i}.alpha(lcurve{i}.chosenindex), lcurve{i}.curvature(lcurve{i}.chosenindex), 'o', 'markersize',4, 'MarkerFaceColor','k','MarkerEdgeColor','none')

end
xlabel('\alpha')
ylabel('curvature')



print('l-curves', '-dpdf') %'-painters', 