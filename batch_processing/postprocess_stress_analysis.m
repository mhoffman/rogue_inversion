% script to compare stresses to crevasses and moulins for inversion study
% area


clear all


%% load domain data to build masks for domain limit and for convex hull


thk=ncread('/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson/rogue_gps_region.cism.nc','thk')';
x1=ncread('/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson/rogue_gps_region.cism.nc','x1');
y1=ncread('/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson/rogue_gps_region.cism.nc','y1');




%%  GPS
% order stns in same order as organize_2011_velo_data.m script
stnlist={'19N1', '22N4', '25N1', '28N4', '33N1', '37N4', '38S3', '41N1', 'FOXX', 'GULL', 'HARE'};

gpswinter=[107.5, 76,     77,    68,      85,     84,      141,   118,     83.5,  77,     109];
% ,69.43818,-49.92776,625,19N1
% ,69.47387,-49.87859,730,22N4
% ,69.44550,-49.78465,836,25N0
% ,69.48642,-49.74294,876,28N4
% ,69.47051,-49.64036,927,33N1
% ,69.52118,-49.60509,988,37N4
% ,69.48456,-49.47901,1006,38S3
% ,69.51898,-49.47063,1039,41N0
% FOX,69.44621,-49.88188,667,22N1 
% GULL,69.45238,-49.71550,852,28N0
% HARE,69.49313,-49.54708,993,37N0

% QING,69.57268,-50.05064,654,XXXX


lat=[
69.43818
69.47387
69.4455
69.48642
69.47051
69.52118
69.48456
69.51898
69.44621
69.45238
69.49313
];

lon=[
-49.92776
-49.87859
-49.78465
-49.74294
-49.64036
-49.60509
-49.47901
-49.47063
-49.88188
-49.7155
-49.54708
];

elev=[
625
730
836
876
927
988
1006
1039
667
852
993
];

[gpsx, gpsy] = polarstereo_fwd(lat, lon, 6378137, 0.0818191908426215, 70.0, -45.0);


[qingx, qingy] = polarstereo_fwd(69.57268, -50.05064, 6378137, 0.0818191908426215, 70.0, -45.0);


%% zero some thickness

% calculate 2d coord matrices
[Xmesh,Ymesh] = meshgrid(x1,y1);

DT = delaunayTriangulation([gpsx, gpsy]);
K = convexHull(DT);
IN = inpolygon(Xmesh, Ymesh, gpsx(K),gpsy(K));  %<-- mask for convex hull of GPS

IN2=IN;

INtemp=IN2;

for s=1:8;  % number of times to grow region
    
    INtemp=IN;
    for sx=[1,0,-1]
        for sy=[1,0,-1]
           IN2 = IN2 + circshift(INtemp, [sx sy]);
        end
    end

end
ind = flipud(IN2)==0;


% get a convex hull for a X km radius around the GPS stn
gpsxBuffer = [];
gpsyBuffer = [];
r = 4000.0 ;  %   <------- the buffer radius for the domain
for s = 1:length(gpsx);
    for a=0:0.05:2*pi
       gpsxBuffer = [gpsxBuffer; gpsx(s) + r * cos(a)];
       gpsyBuffer = [gpsyBuffer; gpsy(s) + r * sin(a)];       
    end
end
DT = delaunayTriangulation([gpsxBuffer, gpsyBuffer]);
K = convexHull(DT);
INbuffer = inpolygon(Xmesh, Ymesh, gpsxBuffer(K),gpsyBuffer(K));    %  <-- mask for domain extent
ind = (flipud(INbuffer)==0);   
    


 
% =========================
% increase RMS outside of convex hull
%ex(IN==0) = ex(IN==0)*2.0;
%ey=ex;
% =========================


% build a mask for within a certain distance to boundary to use to increase
% RMS in INSAR runs to downweight edge regions.
gpsxBuffer = [];
gpsyBuffer = [];
r = 2000.0; %   <-------  buffer radius around GPS beyond which has increased RMS
for s = 1:length(gpsx);
    for a=0:0.05:2*pi
       gpsxBuffer = [gpsxBuffer; gpsx(s) + r * cos(a)];
       gpsyBuffer = [gpsyBuffer; gpsy(s) + r * sin(a)];       
    end
end
DT = delaunayTriangulation([gpsxBuffer, gpsyBuffer]);
K = convexHull(DT);
INbufferINSARcenter = inpolygon(Xmesh, Ymesh, gpsxBuffer(K),gpsyBuffer(K));
  


%IN=flipud(IN);
%INbuffer=flipud(INbuffer);


%% load crevasses
%crev = imread('/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/lauren_crevassing_imagery/crevassing/crev500calcThreshold.tif');
crev = imread('/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/lauren_crevassing_imagery/crevassing/crev500calcThreshold_REVISED_101917.tif');

crev=(crev);
nxcrev=63; nycrev=43;
crevx=[-199036.3200000000069849:500:-199036.3200000000069849+500.0*(nxcrev-1)] +250;  % cell center x
crevy=[-2248860.3700000001117587:500:-2248860.3700000001117587+500*(nycrev-1)] +250;  % cell center y
crevx=crevx';
crevy=fliplr(crevy);
crevy=(crevy)';

figure(1); clf
ax1=subplot(2,2,1); hold all
imagesc(crevx, crevy, (crev>0.5))
colorbar; axis equal


subplot(2,2,2); hold all
imagesc(x1, y1, (thk))
colorbar; 
axis equal


subplot(2,2,3); hold all
imagesc(x1, y1, IN)
colorbar; 
axis equal

subplot(2,2,4); hold all
imagesc(x1, y1, INbuffer)
colorbar; 
axis equal


%% load stresses for different times



% diurnal max
M= csvread( '~/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/178-186-reg2.0_nopressurestress_12-02-16_GIMP/day178.9166/paraview_selection0.csv', 1,1);
strdaymax.x=M(:,11)*1000+250;
strdaymax.y=M(:,12)*1000+250;
strdaymax.R=M(:,8);
strdaymax.s1=M(:,6);
strdaymax.s2=M(:,7);
strdaymax.R=(strdaymax.s1.^2+strdaymax.s2.^2+strdaymax.s1.*strdaymax.s2).^0.5;
strdaymax.raster = 0*thk;
%figure(69); clf; plot(strwin.x, strwin.y, '.')
Fdaymax = scatteredInterpolant(strdaymax.x,strdaymax.y,strdaymax.R);

% summer max
M= csvread( '/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/178-186-reg2.0_nopressurestress_12-02-16_GIMP/paraview_selection_tempstats0.csv', 1,1);
strsummax.x=M(:,41)*1000+250;
strsummax.y=M(:,42)*1000+250;
strsummax.R=M(:,27);
strsummax.raster = 0*thk;
%figure(68); clf; plot(strwin.x, strwin.y, '.')
Fsummax = scatteredInterpolant(strsummax.x,strsummax.y,strsummax.R);



% spring speedup max
M= csvread( '/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/spring_speedup-reg2.0_nopressurestress_08-25-2017_GIMP/paraview_selection0.csv', 1,1);
strsprmax.x=strsummax.x;
strsprmax.y=strsummax.y;
strsprmax.R=M(:,76);
strsprmax.raster = 0*thk;
%figure(68); clf; plot(strwin.x, strwin.y, '.')
Fspring = scatteredInterpolant(strsprmax.x,strsprmax.y,strsprmax.R);



% winter insar
% To create this file:
% run ~/documents/ROGUE/Models/MCB_Jesse_Johnson/batch_processing/get_last_time.py on the exo file
% open
% ~/documents/ROGUE/Models/MCB_Jesse_Johnson/batch_processing/vonmises_singletime.pvsm and replace exo files with the ones you just created
% In Paraview, make new selection of all cells from PointDataToCellData2.
% choose ExtractSelection2 layer, copy current selection, apply
% then from menu: File/Save Data: csv, cell 

% M= csvread( '/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/insar_winter/reg0.05/paraview_selection0.csv', 1,1);
% strwin.x=M(:,11)*1000+250;
% strwin.y=M(:,12)*1000+250;
% strwin.R=M(:,8);
% strwin.s1=M(:,6);
% strwin.s2=M(:,7);
% strwin.R=(strwin.s1.^2+strwin.s2.^2+strwin.s1.*strwin.s2).^0.5;
% strwin.raster = 0*thk;
% figure(67); clf; plot(strwin.x, strwin.y, '.')

% In this file, winter extent is larger than summer, so perform a match to
% only get the parts we need
M= csvread( '/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/insar_winter_rectangular/reg0.05/paraview_selection0.csv', 1,1);
xtemp=M(:,11)*1000+250;
ytemp=M(:,12)*1000+250;
strwin.x=zeros([length(strsummax.x),1]);
strwin.y=zeros([length(strsummax.x),1]);
strwin.s1=zeros([length(strsummax.x),1]);
strwin.s2=zeros([length(strsummax.x),1]);
strwin.R=zeros([length(strsummax.x),1]);
for i=1:length(xtemp)
    for j=1:length(strsummax.x)
       if abs(xtemp(i)- strsummax.x(j)) < 10.0 & abs(ytemp(i)- strsummax.y(j)) < 10.0;
          % copy this one 
          strwin.x(j) = M(i, 11)*1000+250;
          strwin.y(j)=M(i,12)*1000+250;
          strwin.R(j)=M(i,8);
          strwin.s1(j)=M(i,6);
          strwin.s2(j)=M(i,7);
          continue;
       end
    end
end
strwin.R=(strwin.s1.^2+strwin.s2.^2+strwin.s1.*strwin.s2).^0.5;
strwin.raster = 0*thk;
Fwin = scatteredInterpolant(strwin.x(:),strwin.y(:),strwin.R(:), 'linear', 'nearest');


%figure(67); clf; plot(strwin.x, strwin.y, '.')


%% assign crevasse or not to structure
strwin.crev=0*strwin.x;
strwin.crevIN=0*strwin.x;

% x/y positions for rasters below
rX = 0*thk;
rY = 0*thk;

for n=1:length(strwin.x)
   % find the raster cell for this point 
   [junk,i]=min(abs(crevx-strwin.x(n)));
   [junk,j]=min(abs(crevy-strwin.y(n)));
   strwin.crev(n) = crev(j,i) + INbuffer(nycrev+1-j,i)*2 ;% in & crev=3;  out & crev=1; in & not crev=2;  out & not crev=0;
   strwin.crevIN(n) = crev(j,i) + (IN(nycrev+1-j,i)*2);  % in & crev=3;  out & crev=1; in & not crev=2;  out & not crev=0;

    strwin.raster(j,i)=strwin.R(n);   
    strdaymax.raster(j,i)=strdaymax.R(n);   
    strsummax.raster(j,i)=strsummax.R(n);     
    strsprmax.raster(j,i)=strsprmax.R(n);
    rX(j,i) = strsummax.x(n);
    rY(j,i) = strsummax.y(n);
end
for n=1:length(strwin.x)
   % find the raster cell for this point 
   [junk,i]=min(abs(crevx-strwin.x(n)));
   [junk,j]=min(abs(crevy-strwin.y(n)));
   % max in neighboring cells
   strwin.Rneigh(n) = max(max(strwin.raster(j-1:j+1, i-1:i+1)));
end
   
% entire model domain
ind=find(strwin.crev==3);
notind=find(strwin.crev==2);

% inside convex hull only
indcrev=find(strwin.crevIN==3);
indNOTcrev=find(strwin.crevIN==2);

axes(ax1); plot(strwin.x(ind), strwin.y(ind), '.r')


%%%strwin.R=strwin.Rneigh;

% plot histogram
binsize=10;
figure(10); clf; 
axh=subplot(2,1,1); hold on
h2=histogram(strwin.R(notind))
h1=histogram(strwin.R(ind))

h1.BinWidth=binsize;
h2.BinWidth=binsize;
legend('not crevassed', 'crevassed')
xlabel('stress (kPa)')
ylabel('number of modeled cells')
title('entire domain')
xlim([0 300])

% plot histogram for convex hull
axh2=subplot(2,1,2); hold on
h2=histogram(strwin.R(indNOTcrev))
h1=histogram(strwin.R(indcrev))
h1.BinWidth=binsize;
h2.BinWidth=binsize;
legend('not crevassed', 'crevassed')
xlabel('stress (kPa)')
ylabel('number of modeled cells')
title('inside convex hull')
xlim([0 300])
%linkaxes([axh axh2])




%%  set the threshold for the tensile strength
% =============
threshold=140.0;% set this ...
% =============

% plot crevasses and threshold together
figure (20); clf;


subplot(1,2,1); hold all
imagesc(crevx, crevy, ((strwin.raster>threshold)*2) - (strwin.raster<1.0)*1);
colormap([1 1 1; 0.9 0.9 0.9; 0.6 0.6 0.8])  % white, gray, bluish
%colorbar; 
axis equal

%colormap ([[0.9 0.9  0.9]; [ 0.6 0.6 1]])
plot(strwin.x(ind), strwin.y(ind), 'r.')
%plot(strwin.x(notind), strwin.y(notind), 'k.')
legend('crevassed')%, 'uncrevassed')


subplot(1,2,2); hold all
imagesc(crevx, crevy, (strwin.raster>threshold))
colorbar; axis equal
plot(strwin.x(indcrev), strwin.y(indcrev), 'r.')
%plot(strwin.x(indNOTcrev), strwin.y(indNOTcrev), 'k.')



% clean plot for paper
figure (200); clf;
w=8.5; h=3.5;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])

ax=subplot(2,1,1); hold all
set(gca,'FontSize',12)
imagesc(crevx/1000.0, crevy/1000.0, ((strwin.raster>threshold)*2) - (strwin.raster<1.0)*1);
colormap([1 1 1; 0.9 0.9 0.9; 0.6 0.6 0.8])  % white, gray, bluish
plot(strwin.x(ind)/1000, strwin.y(ind)/1000, 'r.')
legend('crevassed','Location','SE')
ylim([-2.247e3 -2.228e3])
ax.XTick=[-200:5:-160];
ax.YTick=[-2250:5:-2220];
ylabel('y position (km)')
xlabel('x position (km)')
axis equal
box on
xlabh = get(gca,'XLabel');
%set(xlabh,'Position',get(xlabh,'Position') - [0 0.5 0])
set(gca, 'Layer','top')
set(gcf,'color','w');
print('crevasse_vs_stress_threshold', '-dpdf')


%% clean plot for paper - histogram + contour 
% figure (201); clf;
% w=8.5; h=5.5;
% set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])

figure (100); clf;
w=10.5; h=3.5;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])
ax=subplot(1,2,1); hold all
set(gca,'FontSize',12)
set(gcf,'color','w');

h2=histogram(strwin.R(notind))
h1=histogram(strwin.R(ind))

h1.BinWidth=binsize;
h2.BinWidth=binsize;
legend('not crevassed', 'crevassed')
xlabel('stress (kPa)')
ylabel('number of modeled grid cells')
xlim([0 300])
box on
print('stress_histogram', '-dpdf')


% ==== the second panel starts here
Ax1=subplot(1,2,2); hold all
set(gca,'FontSize',12)


%ylim([-2.247e3 -2.228e3]); xlim([-198 -174]);
axis equal; 
%ax1.XTick=[-200:5:-160];
%ax1.YTick=[-2250:5:-2220];

box on

imagesc(crevx/1000.0, crevy/1000.0, crev - (strwin.raster<1.0))
%cp=contour(crevx/1000.0, crevy/1000.0, strwin.raster, [110:10:170]);
colormap([1 1 1; 0.9 0.9 0.9; .7 .7 .7])  % white, gray, bluish

%colorbar

% -----  contour plot

Ax2 = axes('Position',get(Ax1,'Position')); 
hold all
set(gca, 'Color', 'none')
%set(gca, 'visible', 'off')
set(gca,'FontSize',12)
%set(gca, 'xlim', get(Ax1, 'xlim'))
%set(gca, 'ylim', get(Ax1, 'ylim'))


ylim([-2.246e3 -2.226e3]); xlim([-198 -170]);
ax2.XTick=[-200:5:-160];
ax2.YTick=[-2250:5:-2220];
set (gca, 'YTick', [-2250:5:-2220])
set(gca, 'Color', 'none')
box on
axis equal; 
 xlim([-198 -170]);

[cp Hp]=contour(crevx/1000.0, crevy/1000.0, strwin.raster, [100:20:200]);
[C H] =contour(crevx/1000.0, crevy/1000.0, strwin.raster, [140,140], 'k:' );  % dotted line on preferred value
set (H, 'LineWidth', 1.5);
mycmap=hsv(6);
colormap(Ax2, mycmap);
%colorbar('Ticks', [100:20:200])

levellist = Hp.LevelList; % get vector of single contours handl.
invisiblelines=zeros([length(levellist) 1]);
for i = 1:length(levellist)
    invisiblelines(i) = plot(NaN(1), NaN(1), 'color', mycmap(i,:));
    legend_entries{i} = num2str(levellist(i));
end
leg=legend(invisiblelines, legend_entries, 'location', 'nw')
leg.FontSize=7;


box on
%plot(strwin.x(ind)/1000, strwin.y(ind)/1000, 'r.')
%legend('crevassed','Location','SE')

ylabel('y position (km)')
xlabel('x position (km)')
axis equal
box on
xlabh = get(gca,'XLabel');
%set(xlabh,'Position',get(xlabh,'Position') - [0 0.5 0])
set(gca, 'Layer','top')
set(gcf,'color','w');


% make the gray image axis match the contour axis
set(Ax1, 'Position',get(Ax2,'Position')); 
set(Ax1, 'xlim', get(Ax2, 'xlim'))
set(Ax1, 'ylim', get(Ax2, 'ylim'))
set(Ax1, 'visible', 'off')

ax2.YTick=[-2250:5:-2220];

print('crevasse_vs_stress_threshold_contour', '-dpdf')


%%

threshold=140.0;% set this ...



% load moulins
%M=csvread('/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/lauren_crevassing_imagery/2011_moulins.csv',1,0);
M=csvread('/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/lauren_crevassing_imagery/2011_moulins_w_lakeassoc.csv',1,0);
mln.x=M(:,1);
mln.y=M(:,2);
mln.lake=M(:,13);


% record stress for different time periods for each moulin
mln.i = 0*mln.x;
mln.j = 0*mln.x;
mln.inDomain = 0*mln.x;
mln.inHull = 0*mln.x;
mln.strwin = 0*mln.x;
mln.strdaymax = 0*mln.x;
mln.strsummax = 0*mln.x;
mln.strsprmax = 0*mln.x;


for n=1:length(mln.x)
    
    
   % find nearest grid cell
   i = floor((mln.x(n) - -199036.3200000000069849) / 500) + 1;
   j = floor((mln.y(n) - [-2248860.3700000001117587]) / 500) + 1;
   mln.i(n)=i; mln.j(n)=j;
   if (i>=1 && i<=nxcrev && j>=1 && j<=nycrev)
      mln.inDomain(n) =  INbuffer(j,i);
      mln.inHull(n) = IN(j,i);
      if mln.inDomain(n)
       %   mln.strwin(n)    = strwin.raster(nycrev+1-j,i);
       %   mln.strdaymax(n) = strdaymax.raster(nycrev+1-j,i);
       %   mln.strsummax(n) = strsummax.raster(nycrev+1-j,i);          
       %   mln.strsprmax(n) = strsprmax.raster(nycrev+1-j,i);   
      end
   end
end

mln.strwin = Fwin(mln.x, mln.y);
mln.strdaymax = Fdaymax(mln.x, mln.y);
mln.strsummax = Fsummax(mln.x, mln.y);
mln.strsprmax = Fspring(mln.x, mln.y);

% get indices for winter
indW=find(mln.strwin >= threshold  & mln.inDomain==1);
notindW=find(mln.strwin < threshold  & mln.inDomain==1);
indWnolake=find(mln.strwin >= threshold  & mln.inDomain==1 & mln.lake==0);
notindWnolake=find(mln.strwin < threshold  & mln.inDomain==1 & mln.lake==0);
indWlake=find(mln.strwin >= threshold  & mln.inDomain==1 & mln.lake==1);
notindWlake=find(mln.strwin < threshold  & mln.inDomain==1 & mln.lake==1);



% get indices for max day
indD=find(mln.strdaymax >= threshold  & mln.inDomain==1);
notindD=find(mln.strdaymax < threshold  & mln.inDomain==1);
indDnolake=find(mln.strdaymax >= threshold  & mln.inDomain==1 & mln.lake==0);
notindDnolake=find(mln.strdaymax < threshold  & mln.inDomain==1 & mln.lake==0);
indDlake=find(mln.strdaymax >= threshold  & mln.inDomain==1 & mln.lake==1);
notindDlake=find(mln.strdaymax < threshold  & mln.inDomain==1 & mln.lake==1);

% get indices for max summer
indS=find(mln.strsummax >= threshold  & mln.inDomain==1);
notindS=find(mln.strsummax < threshold  & mln.inDomain==1);
indSnolake=find(mln.strsummax >= threshold  & mln.inDomain==1 & mln.lake==0);
notindSnolake=find(mln.strsummax < threshold  & mln.inDomain==1 & mln.lake==0);
indSlake=find(mln.strsummax >= threshold  & mln.inDomain==1 & mln.lake==1);
notindSlake=find(mln.strsummax < threshold  & mln.inDomain==1 & mln.lake==1);

% get indices for max spring
indG=find(mln.strsprmax >= threshold  & mln.inDomain==1);
notindG=find(mln.strsprmax < threshold  & mln.inDomain==1);
indGnolake=find(mln.strsprmax >= threshold  & mln.inDomain==1 & mln.lake==0);
notindGnolake=find(mln.strsprmax < threshold  & mln.inDomain==1 & mln.lake==0);
indGlake=find(mln.strsprmax >= threshold  & mln.inDomain==1 & mln.lake==1);
notindGlake=find(mln.strsprmax < threshold  & mln.inDomain==1 & mln.lake==1);

% get indices for lake-associated
indLake=find(mln.lake==1);

% plot each stress field with moulins

figure (22); clf;  % plot 3 stress states at once

subplot(4,1,1); hold all
imagesc(crevx, crevy, (strwin.raster))
colorbar; axis equal
title(['winter: ' num2str(length(indW)/(length(indW)+length(notindW))*100, 3) '% opened; '  num2str(length(indW))])
caxis([0 250])
%caxis([129 130])
plot(mln.x(notindW), mln.y(notindW), 'xk')
plot(mln.x(indW), mln.y(indW), 'ro')


subplot(4,1,3); hold all
imagesc(crevx, crevy, (strdaymax.raster))
colorbar; axis equal
title(['diurnal max: ' num2str(length(indD)/(length(indD)+length(notindD))*100, 3) '% opened; ' num2str(length(indD))])
caxis([0 250])
%caxis([129 130])
plot(mln.x(notindD), mln.y(notindD), 'xk')
plot(mln.x(indD), mln.y(indD), 'ro')

subplot(4,1,4); hold all
imagesc(crevx, crevy, (strsummax.raster))
colorbar; axis equal
title(['lake drainage max: ' num2str(length(indS)/(length(indS)+length(notindS))*100, 3) '% opened; ' num2str(length(indS))])
caxis([0 250])
%caxis([129 130])
plot(mln.x(notindS), mln.y(notindS), 'xk')
plot(mln.x(indS), mln.y(indS), 'ro')


subplot(4,1,2); hold all
imagesc(crevx, crevy, (strsprmax.raster))
colorbar; axis equal
title(['spring speedup: ' num2str(length(indG)/(length(indG)+length(notindG))*100, 3) '% opened; ' num2str(length(indG))])
caxis([0 250])
%caxis([129 130])
plot(mln.x(notindG), mln.y(notindG), 'xk')
plot(mln.x(indG), mln.y(indG), 'ro')

legend('MOSS<140 kPa', 'MOSS>140 kPa')

%% clean plot for paper


figure (222); clf;  % plot 3 stress states at once
fontsize=10;
w=12.5; h=7;
set(gcf, 'Units', 'Inches', 'Position', [0, 0, w, h], 'PaperUnits', 'Inches', 'PaperSize', [w h])
set(gcf,'color','w');


ylimits=[-2.247e3 -2.229e3];
xlimits=[-198 -170];
xticks=[-200:5:-160];
yticks=[-2250:5:-2220];

ax1=subplot(2,2,1); hold all
set(gca,'FontSize',fontsize)
imAlpha=ones(size(strwin.raster));
imAlpha((strwin.raster==0))=0;
imagesc(crevx/1000, crevy/1000, (strwin.raster), 'AlphaData',imAlpha)
%[C H]=contourf(crevx/1000, crevy/1000, strwin.raster, 128, 'LineColor', 'none')
%colorbar; 
box on
[C H]=contour(crevx/1000, crevy/1000, strwin.raster, [1 1]*threshold, 'w');
set (H, 'LineWidth', 1.5);
%title(['winter: ' num2str(length(indW)/(length(indW)+length(notindW))*100, 3) '% opened'] )
caxis([0 250])
%caxis([129 130])
%plot(mln.x(indLake)/1000, mln.y(indLake)/1000, 'k*', 'color', [1 1 1]*0.3)%'.', 'markersize',18, 'color', [1 1 1]*0.5)
%plot(mln.x(notindW)/1000, mln.y(notindW)/1000, 'xk')
%plot(mln.x(indW)/1000, mln.y(indW)/1000, 'ro')
ms=5;
plot(mln.x(notindWnolake)/1000, mln.y(notindWnolake)/1000, 'vk', 'MarkerEdgeColor','k', 'markersize', ms)
plot(mln.x(notindWlake)/1000, mln.y(notindWlake)/1000, 'ok', 'MarkerEdgeColor','k', 'markersize', ms) %'MarkerFaceColor','k',
plot(mln.x(indWnolake)/1000, mln.y(indWnolake)/1000, 'rv', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)
plot(mln.x(indWlake)/1000, mln.y(indWlake)/1000, 'ro', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)


ylim(ylimits); xlim(xlimits);
ax1.XTick=xticks; ax1.YTick=yticks;
axis equal
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')

% l=legend('\sigma_v=140 kPa contour', 'moulin associated with lake drainage', 'moulin where \sigma_v<140 kPa', 'moulin where \sigma_v>140 kPa', ...
%     'interpreter', 'latex', 'color','g', 'location','nw');
% l.Color=[1 1 1]*0.8;
% colormap('parula')
% pos=l.Position;
% set(l, 'Position', [pos(1)-0.01, pos(2)+0.22, pos(3), pos(4)])

% --- summer
ax2=subplot(2,2,3); hold all
set(gca,'FontSize',fontsize)
imagesc(crevx/1000, crevy/1000, (strdaymax.raster), 'AlphaData',imAlpha)
%colorbar; 
box on
[C H]=contour(crevx/1000, crevy/1000, strdaymax.raster, [1 1]*threshold, 'w');
set (H, 'LineWidth', 1.5);
%title(['diurnal max: ' num2str(length(indD)/(length(indD)+length(notindD))*100, 3) '% opened'])
caxis([0 250])
%caxis([129 130])
% plot(mln.x(notindD)/1000, mln.y(notindD)/1000, 'xk')
% plot(mln.x(indD)/1000, mln.y(indD)/1000, 'ro')
% plot(mln.x(indLake)/1000, mln.y(indLake)/1000, 'k+', 'color', [1 1 1]*0.3)%'.', 'markersize',18, 'color', [1 1 1]*0.5)
plot(mln.x(notindDnolake)/1000, mln.y(notindDnolake)/1000, 'vk', 'MarkerEdgeColor','k', 'markersize', ms)
plot(mln.x(notindDlake)/1000, mln.y(notindDlake)/1000, 'ok', 'MarkerEdgeColor','k', 'markersize', ms) %'MarkerFaceColor','k',
plot(mln.x(indDnolake)/1000, mln.y(indDnolake)/1000, 'rv', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)
plot(mln.x(indDlake)/1000, mln.y(indDlake)/1000, 'ro', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)

ylim(ylimits); xlim(xlimits);
ax2.XTick=xticks; ax2.YTick=yticks;
axis equal
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')

% --- lake drainage
ax3=subplot(2,2,4); hold all
set(gca,'FontSize',fontsize)
imagesc(crevx/1000, crevy/1000, (strsummax.raster), 'AlphaData',imAlpha)
%colorbar; 
box on
[C H]=contour(crevx/1000, crevy/1000, strsummax.raster, [1 1]*threshold, 'w');
set (H, 'LineWidth', 1.5);
%title(['lake drainage max: ' num2str(length(indS)/(length(indS)+length(notindS))*100, 3) '% opened'])
caxis([0 250])
%caxis([129 130])
% plot(mln.x(indLake)/1000, mln.y(indLake)/1000, 'k+', 'color', [1 1 1]*0.3)%'.', 'markersize',18, 'color', [1 1 1]*0.5)
% plot(mln.x(notindS)/1000, mln.y(notindS)/1000, 'xk')
% plot(mln.x(indS)/1000, mln.y(indS)/1000, 'ro')
plot(mln.x(notindSnolake)/1000, mln.y(notindSnolake)/1000, 'vk', 'MarkerEdgeColor','k', 'markersize', ms)
plot(mln.x(notindSlake)/1000, mln.y(notindSlake)/1000, 'ok', 'MarkerEdgeColor','k', 'markersize', ms) %'MarkerFaceColor','k',
plot(mln.x(indSnolake)/1000, mln.y(indSnolake)/1000, 'rv', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)
plot(mln.x(indSlake)/1000, mln.y(indSlake)/1000, 'ro', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)

ylim(ylimits); xlim(xlimits);
ax3.XTick=xticks; ax3.YTick=yticks;
axis equal
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')

% -- plot a colorbar - need to  move it with illustrator
ax4 = axes('Position',get(ax3,'Position')); 
set(gca,'FontSize',fontsize)
hold all
set(gca, 'Color', 'none')
set(ax4, 'visible', 'off')
c=colorbar;
position=get(c, 'Position');
set(c, 'Position', [position(1)+0.05, position(2)+0.09, position(3), position(4)*0.78])
caxis([0 250])

% --- spring speedup
ax5=subplot(2,2,2); hold all
set(gca,'FontSize',fontsize)
imagesc(crevx/1000, crevy/1000, (strsprmax.raster), 'AlphaData',imAlpha)
%colorbar; 
box on
[C H]=contour(crevx/1000, crevy/1000, strsprmax.raster, [1 1]*threshold, 'w');
set (H, 'LineWidth', 1.5);
%title(['lake drainage max: ' num2str(length(indS)/(length(indS)+length(notindS))*100, 3) '% opened'])
caxis([0 250])
%caxis([129 130])
% plot(mln.x(indLake)/1000, mln.y(indLake)/1000, 'k+', 'color', [1 1 1]*0.3)%'.', 'markersize',18, 'color', [1 1 1]*0.5)
% plot(mln.x(notindG)/1000, mln.y(notindG)/1000, 'xk')
% plot(mln.x(indG)/1000, mln.y(indG)/1000, 'ro')
plot(mln.x(notindGnolake)/1000, mln.y(notindGnolake)/1000, 'vk', 'MarkerEdgeColor','k', 'markersize', ms)
plot(mln.x(notindGlake)/1000, mln.y(notindGlake)/1000, 'ok', 'MarkerEdgeColor','k', 'markersize', ms) %'MarkerFaceColor','k',
plot(mln.x(indGnolake)/1000, mln.y(indGnolake)/1000, 'rv', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)
plot(mln.x(indGlake)/1000, mln.y(indGlake)/1000, 'ro', 'MarkerFaceColor','r','MarkerEdgeColor','r', 'markersize', ms)

ylim(ylimits); xlim(xlimits);
ax3.XTick=xticks; ax3.YTick=yticks;
axis equal
set(gca, 'Layer','top')
ylabel('y position (km)')
xlabel('x position (km)')

print('stress_vs_moulinx3', '-dpdf')


%% differences from winter
figure (28); clf; 

subplot(2,1,1); hold all
imagesc(crevx, crevy, (strdaymax.raster - strwin.raster))
colorbar; axis equal
title('summer diurnal - winter')
caxis([-1 1]*150)
colormap('jet')

subplot(2,1,2); hold all
imagesc(crevx, crevy, (strsummax.raster - strwin.raster))
colorbar; axis equal
title('lake drainage - winter')
caxis([-1 1]*150)
colormap('jet')
