

import sys
#sys.path.append('/Users/mhoffman/software/trilinos/trilinos-install/lib/')
sys.path.append('/Users/mhoffman/software/seacas-exodus/seacas-exodus/install/lib')
sys.path.append('/Users/mhoffman/software/exomerge')

from exodus import exodus
#from exodus import copy_mesh

# import the module
import exomerge


f1='/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/166-170/day166.2916/rogue_analysis_surface.exo'
# load the model the last timestep
model = exomerge.import_model(f1, timesteps='last')
# save the model to a new file
fout='last_timestep_results.exo'
model.export_model(fout)

# set the time stamp correctly
e1 = exodus(fout, mode='a', array_type='numpy')
e1.put_time(1, 55.6)
e1.close()



f2='/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/166-170/day166.3333/rogue_analysis_surface.exo'
# load the model the last timestep
model = exomerge.import_model(f2, timesteps='last')
# save the model to a new file
fout2='last_timestep_results2.exo'
model.export_model(fout2)

# set the time stamp correctly
e1 = exodus(fout2, mode='a', array_type='numpy')
e1.put_time(1, 77.8)
e1.close()



model = exomerge.import_model(fout)
model.import_model(fout2, element_block_ids='none', node_field_names='all')
model.export_model('combined_mesh.exo')

