#!/bin/bash

# This script runs Albany inversion for every subdir matching day* in a directory
# NOTE: Manually remove (or ignore) any time slices (dirs) you don't want included in the loop!
# source this script so you get env vars needed for albany

# the root dir for all the time slices to get set up in - must exist
BASEDIR=/Users/mhoffman/documents/ROGUE/Models/gps_2011_inversion_workdir/6hr/166-170

# base dir to repo that has the scripts in it
SCRIPTBASEDIR=/Users/mhoffman/documents/ROGUE/Models/MCB_Jesse_Johnson 
#MATLAB=/Applications/MATLAB_R2016a.app/bin/matlab
ALBANYDIR=/Users/mhoffman/software/albany/albany-install/bin

HERE=`pwd`
# start filling out work dir
cd $BASEDIR
date
first=0
for daydir in day*; do
#    [ -d "${path}" ] || continue # if not a directory, skip
    echo $daydir
    cd $daydir
    ncks -O -d time_step,-1 rogue_analysis_surface.exo rogue_analysis_surface.lastiteration.exo
    echo ${daydir:3:11}
    ncap2 -A -s "time_whole(0)=${daydir:3:11}" rogue_analysis_surface.lastiteration.exo

    if [ "$first" = 0 ]; then
      cp rogue_analysis_surface.lastiteration.exo ../rogue_analysis_surface.alltimes.exo
    else
      ncrcat -A rogue_analysis_surface.lastiteration.exo ../rogue_analysis_surface.alltimes.exo ../rogue_analysis_surface.alltimes.exo 
    fi
    first=1
    cd ..
done
cd $HERE
